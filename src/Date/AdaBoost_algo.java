package Date;


import net.peachjean.slf4j.mojo.CurrentMojoLogger;
import smile.classification.AdaBoost;
import smile.classification.SVM;
import smile.data.AttributeDataset;
import smile.data.NominalAttribute;
import smile.data.parser.ArffParser;
import smile.data.parser.DelimitedTextParser;
import smile.data.parser.LibsvmParser;
import smile.math.kernel.GaussianKernel;

import javax.xml.namespace.QName;
import javax.xml.stream.Location;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import java.io.*;
import java.text.ParseException;

public class AdaBoost_algo {





    public static void main(String[] args) throws IOException, ParseException {

//        DelimitedTextParser parser = new DelimitedTextParser();
//        parser.setResponseIndex(new NominalAttribute("class"), 0);
//        AttributeDataset usps = parser.parse("USPS Train", new FileInputStream("/home/avishay/IdeaProjects/Meachine-Learning/Speed Dating Data half.txt"));



        DelimitedTextParser parser = new DelimitedTextParser();
        parser.setResponseIndex(new NominalAttribute("class"), 0);
        try {
            AttributeDataset train = parser.parse("USPS Train", new FileInputStream("/home/avishay/IdeaProjects/Meachine-Learning/zip.train"));
            AttributeDataset test = parser.parse("USPS Test", new FileInputStream("/home/avishay/IdeaProjects/Meachine-Learning/zip.test"));

            double[][] x = train.toArray(new double[train.size()][]);
            int[] y = train.toArray(new int[train.size()]);
            double[][] testx = test.toArray(new double[test.size()][]);
            int[] testy = test.toArray(new int[test.size()]);


            SVM<double[]> svm = new SVM<double[]>(new GaussianKernel(8.0), 5.0, 50, SVM.Multiclass.ONE_VS_ONE);
            svm.learn(x, y);
            svm.finish();

            int error = 0;
            for (int i = 0; i < testx.length; i++) {
                if (svm.predict(testx[i]) != testy[i]) {
                    error++;
                }
            }

           // System.out.format("USPS error rate = %.2f%%\n", 100.0 * error / testx.length);

            System.out.println("USPS one more epoch...");
            for (int i = 0; i < x.length; i++) {
                int j = (int)(Math.random()*x.length);
                svm.learn(x[j], y[j]);
            }

            svm.finish();

            error = 0;
            for (int i = 0; i < testx.length; i++) {
                if (svm.predict(testx[i]) != testy[i]) {
                    error++;
                }
            }
            //System.out.format("USPS error rate = %.2f%%\n", 100.0 * error / testx.length);
        } catch (Exception ex) {
            System.err.println("hey: "+ex);
        }






    }

}
